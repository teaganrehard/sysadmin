#!/usr/bin/python
import os 
import sys
from sys import argv

current_dir = os.getcwd() 
dir_list = os.listdir() 
script, current_ext, new_ext = argv

def remove_dotfiles():
    i = 0
    while i < len(dir_list):
        current_element = dir_list[i]
        if current_element[0] == '.':
            del dir_list[i]
        else:
            i += 1

def process_files():
    i = 0
    while i < len(dir_list):
        current_file = dir_list[i] 
        base = os.path.splitext(current_file)[0]
        iter_ext =  os.path.splitext(current_file)[1]
        if iter_ext == current_ext:
            os.rename(current_file, base + new_ext)
        i += 1

remove_dotfiles()
process_files()
