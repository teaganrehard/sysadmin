#!/usr/bin/python
import os 
import sys

current_dir = os.getcwd() 
dir_list = os.listdir() 

def choose_option():
    print("0. Exit")
    print("1. Remove dotfiles from rename operation.")
    print("2. Process files.")

    choice = input("> ")
    choice = int(choice)

    if choice == 0:
        sys.exit(1)    
    elif choice == 1:
        remove_dotfiles()
        choose_option()
    elif choice == 2:
        process_files()
        choose_option()
    else:
        print("Input not accepted.")
        choose_option()

def remove_dotfiles():
    i = 0
    while i < len(dir_list):
        current_element = dir_list[i]
        if current_element[0] == '.':
            del dir_list[i]
        else:
            i += 1

def process_files():
    current_ext = input("Enter the current file extension: ") 
    new_ext = input("Enter the new file extension: ")

    i = 0
    while i < len(dir_list):
        current_file = dir_list[i] 
        base = os.path.splitext(current_file)[0]
        iter_ext =  os.path.splitext(current_file)[1]
        if iter_ext == current_ext:
            os.rename(current_file, base + new_ext)
        i += 1

choose_option()

