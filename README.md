# Personal Sysadmin Projects
My personal projects that relate to system administration.

## Prerequisites
All Python files are based on Python3

## Deployment
When neccesary, hashbang lines have ben included in the code. If your local python install doesn't call Python3 by default or is installed to a localtion other than /usr/bin, they may need modification.

Other than that, a chmod +x should be all that's necessary. 

## Contributing

I'm new to python, so this probbably isn't great code. If there's something that can be improved, pull requests are greatly appreciated. 

## License

This project is licensed under the GPLv3 [LICENSE.md](LICENSE.md) file for details
